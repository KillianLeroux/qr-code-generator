<?php
namespace App\QR\Image;

use chillerlan\QRCode\Data\QRMatrix;
use chillerlan\QRCode\Output\QRMarkupSVG;

class QRSvgWithLogoAndCustomShapes extends QRMarkupSVG {

    /**
    * @inheritDoc
    */
    protected function paths():string{
        // make sure connect paths is enabled
        $this->options->connectPaths = true;
        // empty the default value to remove the fill* attributes from the <path> elements
        $this->options->markupDark   = '';
        $this->options->markupLight  = '';

        $size = (int)ceil($this->moduleCount * $this->options->svgLogoScale);

        // we're calling QRMatrix::setLogoSpace() manually, so QROptions::$addLogoSpace has no effect here
        $this->matrix->setLogoSpace($size, $size);

        // generate the path element(s) - in this case it's just one element as we've "disabled" several options
        $svg = parent::paths();
        // now we're lazy modifying the generated path to add the custom shapes for the finder patterns
        $svg = str_replace('"/>', $this->getFinderPatterns().'"/>', $svg);
        // and add the custom logo
        $svg .= $this->getLogo();

        return $svg;
    }

    /**
    * returns a path segment for a single module
    *
    * @see https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/d
    */
    protected function module(int $x, int $y, int $M_TYPE):string{

        if(
            !$this->matrix->check($x, $y)
            // we're skipping the finder patterns here
            || $this->matrix->checkType($x, $y, QRMatrix::M_FINDER)
            || $this->matrix->checkType($x, $y, QRMatrix::M_FINDER_DOT)
        ){
            return '';
        }

        // return a heart shape (or any custom shape for that matter)
        return sprintf('M%1$s %2$s m0.5,0.96 l-0.412,-0.412 a0.3 0.3 0 0 1 0.412,-0.435 a0.3 0.3 0 0 1 0.412,0.435Z', $x, $y);
    }

    /**
    * returns a custom path for the 3 finder patterns
    */
    protected function getFinderPatterns():string{

        $qz  = $this->options->addQuietzone ? $this->options->quietzoneSize : 0;
        // the positions for the finder patterns (top left corner)
        // $this->moduleCount includes 2* the quiet zone size already so we need to take this into account
        $pos = [
            [0 + $qz, 0 + $qz],
            [0 + $qz, $this->moduleCount - $qz - 7],
            [$this->moduleCount - $qz - 7, 0 + $qz],
        ];

        // the custom path for one finder pattern - the first move (M) is parametrized, the rest are relative coordinates
        $path   = 'M%1$s,%2$s m2,0 h3 q2,0 2,2 v3 q0,2 -2,2 h-3 q-2,0 -2,-2 v-3 q0,-2 2,-2z m0,1 q-1,0 -1,1 v3 q0,1 1,1 h3 q1,0 1,-1 v-3 q0,-1 -1,-1z m0,2.5 a1.5,1.5 0 1 0 3,0 a1.5,1.5 0 1 0 -3,0Z';
        $finder = [];

        foreach($pos as $coord){
            [$ix, $iy] = $coord;
            $finder[]  = sprintf($path, $ix, $iy);
        }

        return implode(' ', $finder);
    }

    /**
    * returns a <g> element that contains the SVG logo and positions it properly within the QR Code
    *
    * @see https://developer.mozilla.org/en-US/docs/Web/SVG/Element/g
    * @see https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/transform
    */
    protected function getLogo():string{
        // @todo: customize the <g> element to your liking (css class, style...)
        return sprintf(
            '%5$s<g transform="translate(%1$s %1$s) scale(%2$s)" class="%3$s">%5$s	%4$s%5$s</g>',
            ($this->moduleCount - ($this->moduleCount * $this->options->svgLogoScale)) / 2,
            $this->options->svgLogoScale,
            $this->options->svgLogoCssClass,
            file_get_contents($this->options->svgLogo),
            $this->options->eol
        );
    }

}