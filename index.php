<?php
declare(strict_types=1);

require_once './inc/Utils.php';
require_once('./vendor/autoload.php');

use Inc\Utils;
use App\QR\Image\QRImageWithLogo;
use App\QR\Options\LogoOptions;
use chillerlan\QRCode\QRCode;
use chillerlan\QRCode\Common\EccLevel;
use chillerlan\QRCode\Data\QRMatrix;

$url = Utils::getArrayValue('url', $_GET);
$type = Utils::getArrayValue('type', $_GET);
$logo = Utils::getArrayValue('logo', $_GET);

if (!Utils::isUrl($url)) {
    http_response_code(400);

    echo json_encode([
        'message' => 'Please give valid URL in get param "url"'
    ]);

    exit;
}

$options = new LogoOptions([
    'version' => 10,
    'eccLevel' => EccLevel::H,
    'imageBase64' => false,
    'addLogoSpace' => true,
    'logoSpaceWidth' => 13,
    'logoSpaceHeight' => 13,
    'scale' => 10,
    'imageTransparent' => false,
    'drawCircularModules' => true,
    'circleRadius' => 0.45,
    'keepAsSquare' => [QRMatrix::M_FINDER, QRMatrix::M_FINDER_DOT],
    // 'outputType'   => QRCode::OUTPUT_IMAGE_PNG,
    // 'maskPattern' => QRCode::MASK_PATTERN_AUTO,
]);

$qrcode = new QRCode($options);
$qrcode->addByteSegment($url);

$qrOutputInterface = new QRImageWithLogo($options, $qrcode->getMatrix());

$logoPath = __DIR__ . '/assets/img/logo-one-voice.png';

if ($logo === 'l214') {
    $logoPath = __DIR__ . '/assets/img/logo-l214.png';
}

$qrcode = $qrOutputInterface->dump(
    null,
    $logoPath
);

if ($type !== 'render') {

    http_response_code(200);
    
    echo json_encode([
        'base64' => base64_encode($qrcode)
    ]);
    
    exit;
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <title>Génération QR Code</title>
    <link rel="stylesheet" href="./styles.min.css">
</head>
<body>
<h1>QR Code</h1>
<div class="container">
    <img src="data:image/png;base64,<?= base64_encode($qrcode) ?>" alt='QR Code' width='800' height='800'>
</div>
</body>
</html>