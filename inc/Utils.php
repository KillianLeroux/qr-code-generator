<?php
namespace Inc;

/**
 * Class contains usefull functions
 */
class Utils
{
    /**
     * Get Host
     *
     * @return string
     */
    public static function getHost()
    {
        return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
    }

    /**
     * Get current URL
     *
     * @return string
     */
    public static function getCurrentUrl()
    {
        return self::getHost() . $_SERVER['REQUEST_URI'];
    }

    /**
     * Get Request Uri without query string from given $uri or PHP Server request URI
     *
     * @param string $uri
     *
     * @return string
     */
    public static function getRequestUriWithoutQuery($uri = null)
    {
        $uri = is_string($uri) && strlen($uri) > 0 ? $uri : $_SERVER['REQUEST_URI'];

        return parse_url($uri, PHP_URL_PATH);
    }

    /**
     * Check if given param is a not empty string
     *
     * @param mixed $string
     *
     * @return bool
     */
    public static function isNotEmptyString($string)
    {
        return is_string($string) && !empty($string);
    }

    /**
     * Check if $url is a valid URL
     * ATTENTION: URLs with accent aren't valid with this function!!!
     *
     * @param string $url URL to check
     *
     * @return bool
     */
    public static function isUrl($url)
    {
        if (!is_string($url)) {
            return false;
        }

        // To allow URLs with accent
        $path = parse_url($url, PHP_URL_PATH);
        $encoded_path = array_map('urlencode', explode('/', $path));
        $url = str_replace($path, implode('/', $encoded_path), $url);

        return is_string($url) && strlen($url) > 0 && filter_var($url, FILTER_VALIDATE_URL);
    }

    /**
     * Check if $email is a valid Email Address
     *
     * @param string $email Email Address to check
     *
     * @return bool
     */
    public static function isEmail($email)
    {
        return is_string($email) && strlen($email) > 0 && filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    /**
     * Check if given $data string is a valid Base64 string
     *
     * @param string $data
     *
     * @return bool
     */
    public static function isBase64Encoded($data)
    {
        return preg_match('%^[a-zA-Z0-9/+]*={0,2}$%', $data);
    }

    /**
     * Add query parameters to given URL
     *
     * @param string $url
     * @param array $newParams
     *
     * @return string
     */
    public static function addParametersToUrl($url, $newParams)
    {
        $url = parse_url($url);
        parse_str($url['query'] ?? '', $existingParams);

        $newQuery = array_merge($existingParams, $newParams); // Replace params if already exist

        $newUrl = $url['scheme'] . '://' . $url['host'] . ($url['path'] ?? '');

        if ($newQuery) {
            $newUrl .= '?' . http_build_query($newQuery);
        }

        if (isset($url['fragment'])) {
            $newUrl .= '#' . $url['fragment'];
        }

        return $newUrl;
    }

    /**
     * Get property object without error
     *
     * @param string $prop   Property to get
     * @param object $object Object
     *
     * @return mixed|null
     */
    public static function getProp($prop, $object)
    {
        if (is_object($object) && property_exists($object, $prop)) {
            return $object->{$prop};
        }

        return null;
    }

    /**
     * Get value in an array without error
     *
     * @param string $key   Key of the value to get
     * @param array  $array Array
     *
     * @return mixed|null
     */
    public static function getArrayValue($key, $array)
    {
        if (is_int($key) || is_string($key)) {
            if (is_array($array) && array_key_exists($key, $array)) {
                return $array[$key];
            }
        }

        return null;
    }

    /**
     * Redefine keys for given array with value retrieve in each array item (object or array) with given $keyToGet
     *
     * @param int|string $keyToGet
     * @param array $array
     *
     * @return array
     */
    public static function redefineArrayKeys($keyToGet, $array)
    {
        $result = [];

        if (is_array($array)) {
            foreach ($array as $item) {
                if (is_object($item)) {
                    $key = (int) self::getProp($keyToGet, $item);
                } else {
                    $key = (int) self::getArrayValue($keyToGet, $item);
                }
    
                if (is_int($key) && $key > 0) {
                    $result[$key] = $item;
                }
            }
        }

        return $result;
    }

    /**
     * Get key from given value in the given array without error
     *
     * @param string $value Value to check
     * @param array  $array Array
     *
     * @return mixed|null
     */
    public static function getArrayKeyFromValue($value, $array)
    {
        if (!empty($value)) {
            if (is_array($array)) {
                return array_search($value, $array);
            }
        }

        return null;
    }

    /**
     * Get date in string with given format.
     *
     * @param DateTime $date
     * @param string $format
     *
     * @return string
     */
    public static function getDateTimeFormatted($date, $format = 'Y-m-d H:i:s')
    {
        if ($date instanceof \DateTime) {
            return $date->format($format);
        }

        return $date;
    }

    /**
     * Create DateTime from given string
     *
     * @param string $string
     *
     * @return DateTime|boolean
     */
    public static function createDateTimeFromString($string, $timezone = null)
    {
        if (is_string($string) && false !== strtotime($string)) {
            if (is_string($timezone)) {
                date_default_timezone_set($timezone);

                $date = new \DateTime($string);

                return $date->modify('+ ' . (int) date("Z") . ' seconds');
            }

            return new \DateTime($string);
        }

        return false;
    }

    /**
     * Check if given string date is a valid date
     *
     * @param string $stringDate
     * @param string $format
     *
     * @return boolean
     */
    public static function isValidStringDate($stringDate, $format = 'Y-m-d')
    {
        $d = \DateTime::createFromFormat($format, $stringDate);

        return $d && $d->format($format) === $stringDate;
    }

    /**
     * Get values of given column, like array_column() PHP function but with save original key
     *
     * @param array  $array  Array to use
     * @param string $column Column (key) to get
     *
     * @return array
     */
    public static function arrayColumnWithKeys($array, $column)
    {
        $result = [];

        if (is_array($array)) {
            foreach ($array as $key => $item) {
                if (array_key_exists($column, $item)) {
                    $result[$key] = $item[$column];
                }
            }
        }

        return $result;
    }

    /**
     * Convert array to a string list
     *
     * @param array $tab Tab that must be processed
     *
     * @return string
     */
    public static function tabToListString($tab)
    {
        $result = '';
        $comma = '';

        if (is_array($tab)) {
            foreach ($tab as $item) {
                $result .= $comma . $item;
                $comma = ',';
            }
        }

        return $result;
    }

    /**
     * Remove ALL trailing slashes on $string
     *
     * @param string $string
     *
     * @return string|mixed
     */
    public static function removeTrailingSlashes($string)
    {
        if (is_string($string)) {
            return rtrim($string, '/');
        }

        return $string;
    }

    /**
     * Convert all <br> to \r\n
     *
     * @param array $string String that must be processed
     *
     * @return string
     */
    public static function br2nl($string)
    {
        $breaks = array('<br />', '<br>', '<br/>');

        return str_ireplace($breaks, '\n', $string);
    }

    /**
     * Run given URL
     *
     * @param string $url
     *
     * @return void
     */
    public static function runUrl($url, $withoutEcho = false)
    {
        $curl = curl_init($url);

        if ($withoutEcho) {
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        }

        curl_exec($curl);
        curl_close($curl);
    }

    /**
     * Build multipart/form-data string to send in HTTP Post (works with wp_remote_post()).
     * IMPORTANT:
     * HTTP Post have to contain in headers: Content-Type: multipart/form-data; boundary=$boundary
     *
     * @param string|int $boundary Random string/int
     * @param array $fields Array of fields that must be sent
     * @param array $files array of files that must be sent (attachments)
     *
     * @return string
     */
    public static function buildMultipartFormData($boundary, $fields = [], $files = [])
    {
        $result = '';

        $delimiter = "--$boundary";
        $eol = "\r\n";

        if (is_array($fields) && count($fields) > 0) {
            foreach ($fields as $name => $value ) {
                if (is_string($value) || is_numeric($value)) {
                    $result .= $delimiter . $eol;
                    $result .= 'Content-Disposition: form-data; name="' . $name . '"' . "$eol$eol";
                    $result .= $value . $eol;
                }
            }
        }

        if (is_array($files) && count($files) > 0) {
            foreach ($files as $name => $file) {
                // If $file contains all required data
                if (
                    (array_key_exists('tmp_name', $file) || array_key_exists('data', $file))
                    && array_key_exists('type', $file)
                    && array_key_exists('name', $file)
                ) {
                    if (array_key_exists('tmp_name', $file)) {
                        $fileContent = file_get_contents(Utils::getArrayValue('tmp_name', $file));
                    } elseif (array_key_exists('data', $file)) {
                        $fileContent = Utils::getArrayValue('data', $file);

                        if (self::isBase64Encoded($fileContent)) {
                            $fileContent = base64_decode($fileContent);
                        }
                    }

                    $fileType = Utils::getArrayValue('type', $file);
                    $fileName = Utils::getArrayValue('name', $file);

                    $result .= $delimiter . $eol;
                    $result .= 'Content-Disposition: form-data; name="' . $name . '"; filename="' . $fileName . '"' . $eol;
                    $result .= "Content-Type: $fileType$eol$eol";
                    $result .= $fileContent . $eol;
                }
            }
        }

        $result .= "$delimiter--";

        return $result;
    }

    /**
     * Check if given file path is a valid file path (on current server)
     *
     * @param string $filePath File Path to check
     *
     * @return boolean
     */
    public static function isFileExists($filePath)
    {
        return is_string($filePath) && file_exists($filePath);
    }

    /**
     * Check if given file url is a valid file url
     *
     * @param string $fileUrl File URL to check
     *
     * @return bool|null
     */
    public static function isFileExistsFromUrl($fileUrl)
    {
        if (self::isUrl($fileUrl)) {
            $headers = get_headers($fileUrl);

            $httpResponse = (string) self::getArrayValue(0, $headers);

            return strpos($httpResponse, '200 OK') > 0;
        }

        return null;
    }

    /**
     * Remove file from given filePath
     *
     * @param string $filePath
     *
     * @return boolean
     */
    public static function removeFile($filePath)
    {
        if (self::isFileExists($filePath)) {
            return unlink($filePath);
        }

        return false;
    }

    /**
     * Replace extension of given filename by new extension
     *
     * @param string $filename
     * @param string $newExt
     *
     * @return string
     */
    public static function replaceExtension($filename, $newExt) {
        $info = pathinfo($filename);

        return self::getArrayValue('filename', $info) . '.' . $newExt;
    }

    /**
     * Search and replace the first occurence in string of the given $search by the $replace value
     *
     * @param string $subject Subject where search in
     * @param string $search  String to search
     * @param string $replace Replacement string
     *
     * @return void
     */
    public static function strReplaceFirstOccurence($subject, $search, $replace)
    {
        $pos = strpos($subject, $search);

        if ($pos !== false) {
            return substr_replace($subject, $replace, $pos, strlen($search));
        }
    }

    /**
     * Do PHP array_search without case sensitive
     *
     * @param string $search
     * @param array $array
     *
     * @return boolean
     */
    public static function arraySearchInsensitive($search, $array)
    {
        if (is_array($array)) {
            return array_search(strtolower($search), array_map('strtolower', $array)); 
        }

        return false;
    }

    /**
     * Get given URL with http:// if no exists
     *
     * @param string $url
     *
     * @return string
     */
    public static function addHttp($url, $secure = false) {
        if (!is_string($url) || !strlen($url) > 1) {
            return false;
        }

        $s = $secure ? 's' : '';

        if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
            $url = str_replace('//', '', $url);
        
            return "http$s://$url";
        }

        return $url;
    }

    /**
     * htmlspecialchars without double encoding and keep tags
     * Inpired by: https://www.php.net/manual/fr/function.htmlspecialchars.php#101592
     *
     * @param string $html
     *
     * @return string
     */
    public static function htmlSpecialChars($html)
    {
        // Fix weird bug -_-
        $html = preg_replace('/</', "\r\n<", $html);

        $html = htmlspecialchars(
            $html,
            ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML401,
            null,
            false
        );

        $html = preg_replace("/=/", "=\"\"", $html);
        $html = preg_replace("/&quot;/", "&quot;\"", $html);
        $tags = "/&lt;(\/|)(\w*)(\ |)(\w*)([\\\=]*)(?|(\")\"&quot;\"|)(?|(.*)?&quot;(\")|)([\ ]?)(\/|)&gt;/i";
        $replacement = "<$1$2$3$4$5$6$7$8$9$10>";
        $html = preg_replace($tags, $replacement, $html);
        $html = preg_replace('/=\"\"/', '=', $html);

        // Fix exceptions
        while (strpos($html, '&quot;"') !== false) {
            $html = str_replace('&quot;"', '"', $html);
        }

        // Remove double spaces after removing weird bug (see above)
        $html = preg_replace('/\r\n/', '', $html);

        return $html;
    }

    /**
     * Improve file_put_contents PHP function to create directory if not exists
     *
     * @param string $dirPath
     * @param string $filename
     * @param string $content
     *
     * @return int|false
     */
    public static function filePutContentsWithMkDir($dirPath, $filename, $content, $putMode = FILE_APPEND, $chmod = 0755)
    {
        try {
            if (!is_dir($dirPath)) {
                mkdir($dirPath, $chmod, true);
            }
    
            $dirPath = Utils::removeTrailingSlashes($dirPath);
    
            return file_put_contents("$dirPath/$filename", $content, $putMode);
        } catch (\Throwable $th) {
            self::writeLog('all_logs.log', 'filePutContentsWithMkDir', 'Error when put contents', $th);

            return $th;
        }
    }

    /**
     * Write LOGs in file in given $logsFilePath
     *
     * @param string     $logsFilePath
     * @param string     $context
     * @param string     $message
     * @param \Throwable $th
     *
     * @return void
     */
    public static function writeLog($logsFilePath, $context = '', $message = '', $th = null)
    {
        $date = date('Y-m-d H:i:s');

        $errorCode = null;
        $errorMessage = null;
        $errorTrace = null;
        $traces = null;

        $logsFilePath = get_stylesheet_directory() . "/workspace/logs/$logsFilePath"; 

        if ($th instanceof \Throwable) {
            $errorCode = $th->getCode();
            $errorMessage = $th->getMessage();
            $errorTrace = $th->getTraceAsString();

            $traces = <<<EOF
                Code : $errorCode \n
                Message : $errorMessage \n
                Trace : $errorTrace \n\n
            EOF;
        }

        $message = <<<EOF
            ------ LOG $context - $date ------
            $message\n
            $traces
        EOF;

        $logsFilePath = str_replace('\\', '/', $logsFilePath);
        $dirPath = dirname($logsFilePath);

        if (!is_dir($dirPath)) {
            mkdir($dirPath, 0755, true);
        }

        $logsFile = fopen($logsFilePath, 'a');
        fwrite($logsFile, $message);
        fclose($logsFile);
    }

    /**
     * Start PHP Session if not already started
     *
     * @return void
     */
    public static function startSession()
    {
        if (session_status() === PHP_SESSION_NONE) {
            session_start();
        }
    }

     /**
     * Get PHP Session ID
     *
     * @return string
     */
    public static function getSessionId()
    {
        self::startSession();

        return session_id();
    }

    /**
     * Transform a string of values separated by comma into an array
     * 
     * @param string $values
     * 
     * @return array
     */
    public static function prepareFilterMultipleValues($values)
    {
        if (is_string($values) && strlen($values) > 0) {
            return array_map('trim', explode(',', $values));
        }

        return [];
    }
}
